select pg_stat_statements_reset();

create table pss as select ''::text as i, queryid, query, calls, total_exec_time, mean_exec_time, stddev_exec_time, shared_blks_hit, wal_records, wal_bytes from pg_stat_statements where calls > 10 order by total_exec_time ;
\copy pss from 'csv/pss_wti.csv' with (format csv, header)
\copy pss from 'csv/pss_wi.csv' with (format csv, header)

select sum (calls) as calls, sum(total_exec_time) as total, avg(mean_exec_time) as mean,  sum(shared_blks_hit) as hit, sum(wal_records) as wal, sum(wal_bytes) as wal_bytes, i from pss group by i ;

create table psut as select ''::text as i, * from pg_stat_user_tables;
truncate psut;
\copy psut from  'csv/psut_wti.csv' with (format csv, header)
\copy psut from  'csv/psut_wi.csv' with (format csv, header)

select sum(seq_scan) as seq, sum(idx_scan) as idx, sum(n_tup_upd) as upd, sum(n_tup_hot_upd) as upd_hot, sum(n_dead_tup) as dead,  i from psut group by i ;
