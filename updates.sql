
\set id random(1, 1000 )
UPDATE millelignes set ev = now()
   , m = random() * 10000
   , d = md5((random() * 10000)::text)
   , j = jsonb_set( j, '{0,balance}', (random()*10000)::text::jsonb , false )
   where id = :id  ;

\set id random(1, 1000 )
UPDATE millelignes set ev = now()
   , j = jsonb_set( j, '{0,active}', (case when random() > 0.5 then true else false end)::text::jsonb , false )
   where id = :id ;
