# Mille lignes

Test de performances sur une table de mille lignes

Nécessite une instance PostgreSQL 13 avec le droit de créer une base
de données, et l'extension `pg_stat_statements` chargée.

## Lancement

~~~
./init.sh
~~~
