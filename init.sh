#!/bin/bash

DBNAME="millelignes_db"

mkdir -p ./csv/

# CREATE DB
psql <<_EOF_
DROP DATABASE IF EXISTS $DBNAME;
CREATE DATABASE $DBNAME;
\c $DBNAME
CREATE EXTENSION pg_stat_statements;
_EOF_

# CREATE TABLE
psql -d $DBNAME -f createtable.sql
psql -d $DBNAME -c "VACUUM ANALYSE;"

# Test #1
psql -d $DBNAME -c "select pg_stat_reset();"
psql -d $DBNAME -c "select pg_stat_statements_reset();"
pgbench -d $DBNAME -f selects.sql -f updates.sql -T 240 -j 10 -c 10 2> /dev/null

psql -X -d $DBNAME <<_EOF_
\copy (select 'wti', queryid, query, calls, total_exec_time, mean_exec_time, stddev_exec_time, shared_blks_hit, wal_records, wal_bytes  from pg_stat_statements where calls > 10 order by total_exec_time) to 'csv/pss_wti.csv' with (format csv, header)
\copy (select 'wti',* from pg_stat_user_tables) to 'csv/psut_wti.csv' with (format csv, header)
\dt+
\i table_bloat.sql
_EOF_




# CREATE DB
psql <<_EOF_
DROP DATABASE IF EXISTS $DBNAME;
CREATE DATABASE $DBNAME;
\c $DBNAME
CREATE EXTENSION pg_stat_statements;
_EOF_

# CREATE TABLE
psql -d $DBNAME -f createtable.sql

# CREATE INDEX
psql -d $DBNAME -f createindex.sql
psql -d $DBNAME -c "VACUUM ANALYSE;"

# Test #2
psql -d $DBNAME -c "select pg_stat_reset();"
psql -d $DBNAME -c "select pg_stat_statements_reset();"
pgbench -d $DBNAME -f selects.sql -f updates.sql -T 240 -j 10 -c 10 2> /dev/null

psql -X -d $DBNAME <<_EOF_
\copy (select 'wi',queryid, query, calls, total_exec_time, mean_exec_time, stddev_exec_time, shared_blks_hit, wal_records, wal_bytes  from pg_stat_statements where calls > 10 order by total_exec_time) to 'csv/pss_wi.csv' with (format csv, header)
\copy (select 'wi',* from pg_stat_user_tables) to 'csv/psut_wi.csv' with (format csv, header)
\copy (select 'wi',* from pg_stat_user_indexes) to 'csv/psui_wi.csv' with (format csv, header)
\dt+
\di+
\i table_bloat.sql
\i btree_bloat.sql
_EOF_

psql -d $DBNAME -f stats.sql
