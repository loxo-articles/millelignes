CREATE TABLE millelignes
  ( id int,
  ev timestamptz not null default now(),
  m float,
  n float,
  z numeric,
  d text,
  p int8range,
  j jsonb
);

INSERT INTO millelignes
SELECT i , now() , random() * 10000 , random() * 10000
       , round(random() * 1000)::numeric , md5((random() * 10000)::text)
       , int8range( i , i + round(random() * 100)::int8)
       ,('{"bar": "'||md5((random() * 10000)::text)
         ||'", "balance": '||random() * 10000
         ||', "active": '||case when random() > 0.5 then true else false end
         ||', "when": "'||clock_timestamp()||'"}')::jsonb
FROM generate_series(1 , 1000) x (i);
